class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
  end

  def setup
    length = @referee.pick_secret_word
    @guesser.register_secret_length(length)
    @board = Array.new(length)
  end

  def take_turn
    guess = @guesser.guess
    @referee.check_guess(guess)
    self.update_board
    @guesser.handle_response(guess, @board)
  end

  def update_board

  end

end

class HumanPlayer

  def initialize(dictionary)
    @dictionary = dictionary
  end

  def pick_secret_word
    @word = @dictionary.sample
    @word.length
  end

  def check_guess(string)
    raise "Not ONE letter" if string.length > 1
    indices = []
    @word.chars.each_with_index do |ch, i|
      if ch == string
        indices << i
      end
    end
    indices
  end

  def register_secret_length(int)
    @length = int
  end

  def guess(board)
    leftover_lets = ("a".."z").to_a.reject {|ch| board.include?(ch)}
    leftover_lets.sample
  end

  def handle_response(guess, board)

  end

end

class ComputerPlayer
  attr_reader :candidate_words

  def initialize(dictionary)
    @dictionary = dictionary
  end

  def pick_secret_word
    @word = @dictionary.sample
    @word.length
  end

  def check_guess(string)
    raise "Not ONE letter" if string.length > 1
    indices = []
    @word.chars.each_with_index do |ch, i|
      if ch == string
        indices << i
      end
    end
    indices
  end

  def register_secret_length(int)
    # contents = File.readlines("lib/dictionary.txt")
    # contents.map!(&:chomp)
    contents = @dictionary
    @candidate_words = contents.select {|word| word.length == int}
  end

    # To generate a guess, a fair strategy is to guess the most frequent
    # letter in the subset of possible words. If the guessed letter is
    # found, filter the dictionary to only words with the guessed letter in
    # the correct position(s).

  def guess(board)
    leftover_lets = ("a".."z").to_a.reject {|ch| board.include?(ch)}
    leftover_lets.sample
  end

  # Filter out words that have the guessed letter at an incorrect
  # position. In particular, if the guessed letter is not present,
  # eliminate all words in the dictionary which feature the letter.

  # guesser = ComputerPlayer.new(["leer", "reel", "real", "rear"])
  # guesser.register_secret_length(4)
  #
  # guesser.handle_response("r", [0])
  #
  # expect(guesser.candidate_words.sort).to eq(["reel","real"].sort)


  def handle_response(guess_let, response_indices)
    #reject a word if
    if @candidate_words != nil
      @candidate_words.select! do |word|
        response_indices.all? {|ind| word[ind] == guess_let}
      end
      @candidate_words.select! do |word|
        word.chars.each_with_index do |ch,i|
          if ch == guess_let && !response_indices.include?(i)
            return false
          end
        end
        true
      end
      # if response_indices == []
      #   @candidate_words.reject! do |word|
      #     word.include? (guess_let)
      #   end
      # end
    end
  end


end
